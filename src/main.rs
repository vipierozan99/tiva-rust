#![no_std]
#![no_main]

extern crate panic_halt; // panic handler

use cortex_m_rt::entry;
use tm4c123x_hal as hal;
use tm4c123x_hal::prelude::*;
use tm4c123x_hal::sysctl;



#[entry]
fn main() -> ! {
    let p = hal::Peripherals::take().unwrap();
    let cp = hal::CorePeripherals::take().unwrap();
    let mut sc = p.SYSCTL.constrain();
    sc.clock_setup.oscillator = sysctl::Oscillator::Main(
        sysctl::CrystalFrequency::_16mhz,
        sysctl::SystemClock::UsePll(sysctl::PllOutputFrequency::_80_00mhz),
    );
    let clocks = sc.clock_setup.freeze();
    let mut delay = hal::delay::Delay::new(cp.SYST,&clocks);

    let portf = p.GPIO_PORTF.split(&sc.power_control);
    let mut red_led = portf.pf1.into_push_pull_output();
    let mut red_blue = portf.pf2.into_push_pull_output();
    let mut red_green = portf.pf3.into_push_pull_output();

    loop {
        red_led.set_high();
        delay.delay_ms(500u32);
        red_led.set_low();
        // red_green.set_high();
        delay.delay_ms(500u32);
        // red_green.set_low();
        // red_blue.set_high();
        // delay.delay_ms(500u32);
        // red_blue.set_low();
        // delay.delay_ms(500u32);
    }
}